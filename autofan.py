import sensors
import time
import os


ADAPTER_NAME = 'Virtual device'
FREQ = 3    # check every FREQ seconds
RANGES = [{'min':43,'max':45,'lvl':'1'},
          {'min':46,'max':47,'lvl':'2'},
          {'min':48,'max':50,'lvl':'3'},
          {'min':50,'max':56,'lvl':'4'}]
          #{'min':53,'max':56,'lvl':'6'}]

sensors.init()
try:
    while True:
        for chip in sensors.iter_detected_chips():
            if chip.adapter_name == ADAPTER_NAME:
                for feature in chip:
                    temp = feature.get_value()
                    fan_change = False
                    for rnge in RANGES:
                        if rnge['min'] <= temp <= rnge['max']:
                            fan_change = True
                            os.system('echo level ' + rnge['lvl'] + ' > /proc/acpi/ibm/fan')
                            break
                    if fan_change:
                        print '{0} {1} --> setting fan lvl to {2}'.format(feature.label, feature.get_value(),rnge['lvl'])
                    else:
                        print '{0} {1}'.format(feature.label, feature.get_value())

        time.sleep(FREQ)
finally:
    sensors.cleanup()
