# AutoFan#

**DISCLAIMER: Altering your fan speed can cause damage to your computer, use it at your own risk**

Autofan is used to control your thinkpad fan on linux (it can be used for other laptops with little change).
it is used to solve heating problems - if you want your fan to start working at lower temperatures.

I have created this after having my thinkpad heating too much. i realized that the fan was starting to work only after the temp reached 57. this script start the fan at 43 degrees and increase the fan speed as the temp goes higher.
the values can be changed by editing the list in the autofan.py file.

###  Prerequsites ###

install lm-sensors (ubuntu)

```
#!bash

sudo apt-get install lm-sensors
```

create file thinkpad_acpi.conf inside /etc/modprobe.d/

```
#!bash

sudo nano /etc/modprobe.d/thinkpad_acpi.conf
```

add this line to the file and save it
```
#!bash

options thinkpad_acpi fan_control=1
```

now restart your computer

### Usage ###

open terminal 

```
#!bash

sudo python autofan.py
```

That's it. the fan will start working on lower temperatures and hopefully that will solve your heating problem.
you can see your current fan speed using

```
#!bash

sudo watch cat /proc/acpi/ibm/fan
```